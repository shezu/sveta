<?php

const VIEWS_DIR = __DIR__ . '/views';

function view($view, array $params = [])
{
    foreach ($params as $key => $val)
    {
        $$key = $val;
    }
    include_once VIEWS_DIR . '/' . $view . '.php';
}
