<?php

class MethodKlaster
{
    private $C, $d = [], $U = [], $Uold = [], $J = 0, $Jold = 0;
    public $NR, $NC = 1, $NP;
    private $students = [];
    private $subjects = [];

    public function loadStudentsData()
    {
        $data = [];
        global $pdo;
        $pdo->exec('SET NAMES utf8');
        $s = $pdo->prepare('select * from `' . TABLE_STUDENTS . '`');
        if($s->execute())
        {
            $rows = $s->fetchAll();
            foreach($rows as $row)
            {
                $student_id = intval($row['id']);
                $smarks = $pdo->prepare('SELECT DISTINCT `student_id`, `subj_id`, `mark` FROM `' . TABLE_MARKS . '` WHERE `student_id` = :id');
                $this->students[] = [
                    'id' => $row['id'],
                    'fio' => $row['fio'],
                ];
                if($smarks->execute([':id' => $student_id]))
                {
                    $marks = $smarks->fetchAll();
                    foreach($marks as $mark)
                    {
                        $subj_id = intval($mark['subj_id']);
                        $mark = intval($mark['mark']);
                        $data[$student_id][$subj_id] = $mark;
                    }
                }
            }
        }
        $result = $this->normalizeStudentsData($data);
        return $result;
    }

    private function normalizeStudentsData($data)
    {
        $this->NR = count($data);
        global $pdo;
        $result = [];
        $s = $pdo->prepare('select * from `' . TABLE_SUBJECT . '`');
        if($s->execute())
        {
            $subjects = $s->fetchAll();
            foreach($data as $student_id => $student)
            {
                for($i = 0; $i < count($subjects); $i++)
                {
                    $subj_id = intval($subjects[$i]['id']);
                    $result[$student_id][$subj_id] = (isset($data[$student_id][$subj_id]) ? $data[$student_id][$subj_id] : 0);
                }
            }
            $this->NP = count($subjects);
        }
        return $result;
    }


    public function loadSubjectsData()
    {
        $data = [];
        global $pdo;
        $pdo->exec('SET NAMES utf8');
        $s = $pdo->prepare('select * from `' . TABLE_SUBJECT . '`');
        if($s->execute())
        {
            $rows = $s->fetchAll();
            foreach($rows as $row)
            {
                $subject_id = intval($row['id']);
                $smarks = $pdo->prepare('SELECT * FROM `' . TABLE_MARKS . '` WHERE `subj_id` = :id');
                $this->subjects[] = [
                    'id' => $row['id'],
                    'name' => $row['name'],
                ];
                if($smarks->execute([':id' => $subject_id]))
                {
                    $marks = $smarks->fetchAll();
                    foreach($marks as $mark)
                    {
                        $student_id = $mark['student_id'];
                        $mark = intval($mark['mark']);
                        $data[$subject_id][$student_id] = $mark;
                    }
                }
            }
        }
        $result = $this->normalizeSubjectsData($data);
        return $result;
    }

    private function normalizeSubjectsData($data)
    {
        $this->NR = count($data);
        global $pdo;
        $result = [];
        $s = $pdo->prepare('select * from `' . TABLE_STUDENTS . '`');
        if($s->execute())
        {
            $students = $s->fetchAll();
            foreach($data as $subject_id => $subject)
            {
                for($i = 0; $i < count($students); $i++)
                {
                    $subj_id = intval($students[$i]['id']);
                    $result[$subject_id][$subj_id] = (isset($data[$subject_id][$subj_id]) ? $data[$subject_id][$subj_id] : 0);
                }
            }
            $this->NP = count($students);
        }
        return $result;
    }

    public function clustering(array $M)
    {
        $this->U = [];
        for($i = 0; $i < 18; $i++)
        {
            $this->U[$i][0] = 1;
            $this->U[$i][1] = 0;
        }
        for($i = 18; $i < $this->NR; $i++)
        {
            $this->U[$i][0] = 0;
            $this->U[$i][1] = 1;
        }
        $this->Jold = 0;
        $this->J = 0;
        $this->C = [];
        $this->d = [];
        $this->Uold = [];

        do
        {
            $this->NC++;
            $this->Jold = $this->J;
            do
            {
                //centers of clasters определение центров кластеров
                for($i = 0; $i < $this->NC; $i++)
                {
                    for($k = 0; $k < $this->NP; $k++)
                    {
                        $sum = 0;
                        $sum2 = 0;
                        for($j = 0; $j < $this->NR; $j++)
                        {
                            $sum +=
                                $this->U[$j][$i] *
                                $this->U[$j][$i] *
                                $M[$j][$k];
                            $sum2 += $this->U[$j][$i] * $this->U[$j][$i];
                        }
                        if(abs($sum2) > 0.00001)
                        {
                            $this->C[$i][$k] = $sum / $sum2;
                        }
                        else
                        {
                            $this->C[$i][$k] = 0;
                        }
                    }
                }
                // distance расстояние объекта от центра кластера
                for($i = 0; $i < $this->NC; $i++)
                {
                    for($j = 0; $j < $this->NR; $j++)
                    {
                        $sum = 0;
                        for($k = 0; $k < $this->NP; $k++)
                        {
                            $sum += ($M[$j][$k] - $this->C[$i][$k]) * ($M[$j][$k] - $this->C[$i][$k]);
                        }
                        $this->d[$j][$i] = (float)sqrt($sum);
                    }
                }
                // remember  U запоминаем старое значение
                for($i = 0; $i < $this->NR; $i++)
                {
                    for($l = 0; $l < $this->NC; $l++)
                    {
                        $this->Uold[$i][$l] = $this->U[$i][$l];
                    }
                }
                // U пресчитываем новое значение
                for($i = 0; $i < $this->NR; $i++)
                {
                    for($j = 0; $j < $this->NC; $j++)
                    {
                        $sum = 0;
                        for($l = 0; $l < $this->NC; $l++)
                        {
                            $sum += ($this->d[$i][$j] * $this->d[$i][$j]) / ($this->d[$i][$l] * $this->d[$i][$l]);
                        }
                        $this->U[$i][$j] = 1 / ($sum);
                    }
                }
                // max difference DU   максимальная разница по всем ячейкам новой и старой матрицы U
                $DU = 0;
                for($i = 0; $i < $this->NR; $i++)
                {
                    for($j = 0; $j < $this->NC; $j++)
                    {
                        if($DU < abs($this->U[$i][$j] - $this->Uold[$i][$j]))
                        {
                            $DU = abs($this->U[$i][$j] - $this->Uold[$i][$j]);
                        }
                    }
                }

            } while($DU > (float)0.05); //iterations итерации пока  разница не бдет маленькой

            $sum = 0;//aim function функцыя цели минимальная при опт числе кластеров
            // при уточнении обучающей выборки число кластеров не меняется это можно опустить
            for($i = 0; $i < $this->NR; $i++)
            {
                for($j = 0; $j < $this->NC; $j++)
                {
                    $sum += $this->U[$i][$j] * $this->U[$i][$j] * $this->d[$i][$j] * $this->d[$i][$j];
                }
            }
            $this->J = $sum;
            if($this->Jold == 0)
            {
                $this->Jold = $this->J + 1;
            }
        } while($this->J < $this->Jold && $this->NC != 3);// clasters number optimization

        for($i = 0; $i < $this->NR; $i++)
        {
            $max0 = $this->U[$i][0];
            $jj = 0;
            for($j = 0; $j < $this->NC; $j++)
            {
                if($this->U[$i][$j] > $max0)
                {
                    $max0 = $this->U[$i][$j];
                    $jj = $j;
                }
            }

            for($j = 0; $j < $this->NC; $j++)
            {
                $this->U[$i][$j] = 0;
            }
            $this->U[$i][$jj] = 1;
        }
    }

    public function showResult()
    {
        $st = "Center of clasters:</br>";
        for($i = 0; $i < $this->NC - 1; $i++)
        {
            for($j = 0; $j < $this->NP; $j++)
            {
                $st .= $this->C[$i][$j] . " ";
            }
            $st .= "</br>";
        }
        return $st;
    }

    public function showResult1()
    {
        $st = "U:</br>";
        for($i = 0; $i < $this->NR; $i++)
        {
            for($j = 0; $j < $this->NC; $j++)
            {
                $st .= $this->U[$i][$j] . " ";
            }

            $st .= "</br>";
        }
        return $st;
    }

    public function getStudentsResult1()
    {
        $result = [];
        for($i = 0; $i < $this->NR; $i++)
        {
            $item = [];
            $item['student'] = $this->students[$i]['fio'];
            for($j = 0; $j < $this->NC; $j++)
            {
                $item['u'][] = $this->U[$i][$j];
            }
            $result[] = $item;
        }
        return $result;
    }

    public function getSubjectsResult1()
    {
        $result = [];
        for($i = 0; $i < $this->NR; $i++)
        {
            $item = [];
            $item['subject'] = $this->subjects[$i]['name'];
            for($j = 0; $j < $this->NC; $j++)
            {
                $item['u'][] = $this->U[$i][$j];
            }
            $result[] = $item;
        }
        return $result;
    }

    public function getMaxU()
    {
        $result = [];
        for($i = 0; $i < $this->NR; $i++)
        {
            $max_value = 0;
            for($j = 0; $j < $this->NC; $j++)
            {
                if($this->U[$i][$j] >= $max_value)
                {
                    $max_value = $this->U[$i][$j];
                }
            }
            $result[] = $max_value;
        }
        return $result;
    }

    public function getStudentMaxU()
    {
        $result = [];
        $maxUs = $this->getMaxU();
        if(count($maxUs) == count($this->students))
        {
            for($i = 0; $i < count($this->students); $i++)
            {
                $result[] = [
                    'student' => $this->students[$i]['fio'],
                    'maxU' => $maxUs[$i],
                ];
            }
        }
        return $result;
    }
}