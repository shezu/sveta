<?php

include_once './pdo.php';
include_once './tables.php';
include_once './core.php';
include_once './functions.php';
include_once './MethodKlaster.php';

$cluster = new MethodKlaster();
$data = $cluster->loadSubjectsData();
$cluster->clustering($data);
print_r(json_encode($cluster->getSubjectsResult1(), JSON_UNESCAPED_UNICODE));
return;