<?php

include_once './config.php';

try
{
    $pdo_dsn = 'mysql:dbname=' . $db_name . ';host=' . $db_host . ';';
    $pdo = new PDO($pdo_dsn, $db_user, $db_pass);
} catch(PDOException $e)
{
    echo $e->getMessage();
    exit(-1);
}
